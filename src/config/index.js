export const MenuNameCodeMap = {
    '通知公告': 'v_notice_test',
    '请假审批': 'v_leave',
    '电教报修': 'v_dev_repair',
    '总务报修': 'v_dev_repair',
    '印制审批': 'v_req_printing',
    '印制办理': 'v_do_printing',
    '代课管理': 'v_daike',
    '调课管理': 'v_adjust_course',
    '巡堂检查': 'v_check_notice',
    '教务安排': 'v_edu_notice',
    '早读反馈': 'v_er_notice',
    '教师培训': 'v_train_notice',
    '随堂听课': 'v_vc_notice',
    '辅导学生': 'v_coach_notice',
    '教研安排': 'v_tplan_notice',
    '德育打分': 'v_edu_score_detail',
    '护导反馈': 'v_hd',
    '班级考勤': 'v_class_notice'
};

export const SchoolNewsTypeInterfaceMap = {
    1: 'grid/query/t_notice-v_notice',
    2: 'grid/query/o_check_notice-v_check_notice',
    3: 'grid/query/o_edu_notice-v_edu_notice',
    4: 'grid/query/o_er_notice-v_er_notice',
    5: 'grid/query/o_train_notice-v_train_notice',
    6: 'grid/query/o_vc_notice-v_vc_notice',
    7: 'grid/query/o_coach_notice-v_coach_notice',
    8: 'grid/query/o_tplan_notice-v_tplan_notice',
};

export const SchoolNewsDetailInterfaceMap = {
    1: 'form/detailJson/t_notice-${id}?template=lay',
    2: 'form/detailJson/t_notice-${id}?template=lay',
    3: 'form/detailJson/t_notice-${id}?template=lay',
    4: 'form/detailJson/t_notice-${id}?template=lay',
    5: 'form/detailJson/t_notice-${id}?template=lay',
    6: 'form/detailJson/t_notice-${id}?template=lay',
    7: 'form/detailJson/t_notice-${id}?template=lay',
    8: 'form/detailJson/t_notice-${id}?template=lay'
};
