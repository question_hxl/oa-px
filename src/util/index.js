export function groupBy(list, key){
    let resultMap = {};
    let resultList = [];
    list.forEach(item => {
        let list = resultMap[item[key]];
        if(list && list.length > 0) {
            list.push(item);
        }else {
            resultMap[item[key]] = [item];
        }
    });
    for(let i in resultMap) {
        resultList.push({
            name: i,
            list: resultMap[i]
        });
    }
    return resultList;
}

export function getUrlParam(){
    var url = location.search; //获取url中"?"符后的字串
    var theRequest = new Object();
    if (url.indexOf("?") !== -1) {
        var str = url.substr(1);
        var strs = str.split("&");
        for(var i = 0; i < strs.length; i ++) {
            theRequest[strs[i].split("=")[0]] = strs[i].split("=")[1];
        }
    }
    return theRequest;
}