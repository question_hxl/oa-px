import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'

Vue.use(Router);

const router =  new Router({
    routes: [
        {
            path: '/',
            name: 'home',
            component: Home
        },
        {
            path: '/about',
            name: 'about',
            // route level code-splitting
            // this generates a separate chunk (about.[hash].js) for this route
            // which is lazy-loaded when the route is visited.
            component: () => import(/* webpackChunkName: "about" */ './views/About.vue')
        }, {
            path: '/school-event',
            name: 'SchoolEvent',
            component: () => import(/* webpackChunkName: "schoolEvent" */ './views/SchoolEvents.vue')
        }, {
            path: '/school-event-list',
            name: 'SchoolEventList',
            component: () => import(/* webpackChunkName: "schoolEventList" */ './views/SchoolEventList.vue')
        }, {
            path: '/add-school-news',
            name: 'AddSchoolNews',
            component: () => import(/* webpackChunkName: "AddSchoolNews" */ './views/AddSchoolNews.vue')
        }, {
            path: '/school-event-detail/:id',
            name: 'SchoolEventDetail',
            component: () => import(/* webpackChunkName: "schoolEventDetail" */ './views/SchoolEventDetail.vue')
        }, {
            path: '/leave-approval',
            name: 'LeaveApproval',
            component: () => import(/* webpackChunkName: "LeaveApproval" */ './views/LeaveApproval.vue')
        }, {
            path: '/add-leave-application',
            name: 'AddLeaveApplication',
            component: () => import(/* webpackChunkName: "AddLeaveApplication" */ './views/AddLeaveApplication.vue')
        }, {
            path: '/repair/:type',
            name: 'Repair',
            component: () => import(/* webpackChunkName: "Repair" */ './views/Repair.vue')
        }, {
            path: '/add-repair/:type',
            name: 'AddRepair',
            component: () => import(/* webpackChunkName: "AddRepair" */ './views/AddRepair.vue')
        }, {
            path: '/print',
            name: 'Print',
            component: () => import(/* webpackChunkName: "Print" */ './views/PrintApproval.vue')
        }, {
            path: '/take-over-class',
            name: 'TakeOverClass',
            component: () => import(/* webpackChunkName: "TakeOverClass" */ './views/TakeOverClass.vue')
        }, {
            path: '/add-take-over-class',
            name: 'AddTakeOverClassApply',
            component: () => import(/* webpackChunkName: "AddTakeOverClassApply" */ './views/AddTakeOverClassApply.vue')
        }, {
            path: '/change-class',
            name: 'ChangeClass',
            component: () => import(/* webpackChunkName: "ChangeClass" */ './views/ChangeClass.vue')
        }, {
            path: '/add-change-class',
            name: 'AddChangeClassApply',
            component: () => import(/* webpackChunkName: "AddChangeClass" */ './views/AddChangeClass.vue')
        }, {
            path: '/moral-score',
            name: 'MoralScore',
            component: () => import(/* webpackChunkName: "MoralEduScore" */ './views/MoralEduScore.vue')
        }, {
            path: '/edit-moral-score',
            name: 'EditMoralEduScore',
            component: () => import(/* webpackChunkName: "EditMoralEduScore" */ './views/EditMoralEduScore.vue')
        }, {
            path: '/protection',
            name: 'Protection',
            component: () => import(/* webpackChunkName: "Protection" */ './views/Protection.vue')
        }, {
            path: '/protection-detail/:id',
            name: 'ProtectionDetail',
            component: () => import(/* webpackChunkName: "ProtectionDetail" */ './views/ProtectionDetail.vue')
        }, {
            path: '/create-protection',
            name: 'CreateProtection',
            component: () => import(/* webpackChunkName: "CreateProtection" */ './views/CreateProtection.vue')
        }, {
            path: '/examine-protection/:id',
            name: 'ProtectionExamine',
            component: () => import(/* webpackChunkName: "ProtectionExamine" */ './views/ProtectionExamine.vue')
        }, {
            path: '/feedback-protection/:id',
            name: 'ProtectionFeedback',
            component: () => import(/* webpackChunkName: "ProtectionFeedback" */ './views/ProtectionFeedback.vue')
        }, {
            path: '/score-statistics',
            name: 'ScoreStatistics',
            component: () => import(/* webpackChunkName: "ScoreStatistics" */ './views/ScoreStatistics.vue')
        }, {
            path: '/class-attendance',
            name: 'ClassAttendance',
            component: () => import(/* webpackChunkName: "ClassAttendance" */ './views/ClassAttendance.vue')
        }, {
            path: '/edit-class-attendance',
            name: 'EditClassAttendance',
            component: () => import(/* webpackChunkName: "EditClassAttendance" */ './views/EditClassAttendance.vue')
        }

    ]
});

router.afterEach(()=> {
    setTimeout(() => {
        if(document.documentElement) {
            document.documentElement.scrollTop = 0;
        } else {
            document.body.scrollTop = 0;
        }
    }, 100);
});

export default router;
