export default {
    data() {
        return {
            userMenuRights: []
        };
    },
    computed: {
        hasAddRights() {
            return this.userMenuRights.indexOf('ADD') !== -1;
        },
        hasModifyRights() {
            return this.userMenuRights.indexOf('MODIFY') !== -1;
        },
        hasDeleteRights() {
            return this.userMenuRights.indexOf('DEL') !== -1;
        },
        hasHandleRights(){
            return this.userMenuRights.indexOf('HANDLE') !== -1;
        }
    },
    methods: {
        getUserRights(menuCode) {
            if (window.userInfo) {
                let menuList = window.userInfo.sunmenu;
                let menu = menuList.find(item => item.code === menuCode);
                let addRights = menu.button.find(item => item.name === '新增');
                let modifyRights = menu.button.find(item => item.name === '修改');
                let delRights = menu.button.find(item => item.name === '删除');
                let handleRights = menu.button.find(item => {
                    return (item.name === '办理'
                    || item.name === '审核'
                    || item.name === '维修'
                    || item.name === '考核');
                });
                let rights = [];
                if(addRights) {
                    rights.push('ADD');
                }
                if(modifyRights) {
                    rights.push('MODIFY');
                }
                delRights && rights.push('DEL');
                handleRights && rights.push('HANDLE');
                this.userMenuRights = rights;
                return rights;
            } else {
                return [];
            }
        }
    }
}