import Vue from 'vue';
import MintUI from 'mint-ui'
import 'mint-ui/lib/style.css'
import App from './App.vue';
import router from './router';
import axios from 'axios';
import VueAxios from 'vue-axios';
import PortalVue from 'portal-vue/dist/portal-vue.min';
import 'vodal/common.css';
import 'vodal/zoom.css';
import {getUrlParam} from "./util";

import './assets/scss/index.scss';

Vue.config.productionTip = false;

Vue.use(MintUI);
Vue.use(VueAxios, axios);
Vue.use(PortalVue);

let qs = getUrlParam();

Vue.axios.get(window.baseConfig.httpUrl + 'doAuthorizeLogin', {
  params: {
    username: qs.userId
  }
}).then(res => {
  if(res.data.status === 200) {
    window.userInfo = res.data.data;
      new Vue({
          router,
          render: h => h(App)
      }).$mount('#app');
  }else {
    window.userInfo = null;
    alert('获取用户信息失败！');
  }
}, () => {
    window.userInfo = null;
});
