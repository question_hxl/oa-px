module.exports = {
    devServer: {
        proxy: {
            '/api': {
                target: 'http://sxoaapp.pksyxx.cn/',
                ws: true,
                changeOrigin: true
            }
        }
    }
}
